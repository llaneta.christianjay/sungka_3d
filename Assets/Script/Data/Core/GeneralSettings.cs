using Sungka.Controller.Sungka;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sungka.Data.Core
{
    [CreateAssetMenu(menuName = "Scriptable_Objects/Core/GeneralSettings")]
    public class GeneralSettings : ScriptableObject
    {
        #region config
        [SerializeField] Shell shellPrefab;
        [SerializeField] List<Color> shellColors = new();
        [SerializeField] int defaultStartingShell = 7;
        [SerializeField] float shellDropSpeedDuration = 1;
        [SerializeField] LayerMask sungkaContainerLayer;
        #endregion

        public Shell GetShellPrefab()
        {
            return shellPrefab;
        }

        public List<Color> GetShellColors()
        {
            return shellColors;
        }

        public int GetDefaultStartingShell()
        {
            return defaultStartingShell;
        }

        public float GetShellDropSpeedDuration()
        {
            return shellDropSpeedDuration;
        }

        public LayerMask GetSungkaContainerLayer()
        {
            return sungkaContainerLayer;
        }
    }

}
