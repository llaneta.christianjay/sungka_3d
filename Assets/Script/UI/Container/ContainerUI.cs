using Sungka.Controller.Sungka;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sungka.UI.Container
{
    public class ContainerUI : MonoBehaviour
    {
        #region Config
        [SerializeField] UserInterface containerInterface;
        [SerializeField] ContainerDetailUI containerDetailPrefab;
        [SerializeField] Transform player1ContainerUI;
        [SerializeField] Transform player2ContainerUI;
        [SerializeField] ContainerDetailUI player1ContainerScore;
        [SerializeField] ContainerDetailUI player2ContainerScore;
        [SerializeField] SungkaBoardController boardController;
        #endregion

        private void Awake()
        {
            containerInterface = GetComponent<UserInterface>();
            boardController.OnBoardPopulated += AssignedContainerDetails;
        }

        private void AssignedContainerDetails()
        {
            foreach (SungkaContainer container in boardController.GetThePlayer1Container())
            {
                ContainerDetailUI containerUI = Instantiate(containerDetailPrefab, player1ContainerUI);
                containerUI.AssiganedContainer(container);
            }
            List<SungkaContainer> playerContainer = boardController.GetThePlayer2Container();
            playerContainer.Reverse();

            foreach (SungkaContainer container in playerContainer)
            {
                ContainerDetailUI containerUI = Instantiate(containerDetailPrefab, player2ContainerUI);
                containerUI.AssiganedContainer(container);
            }

            player1ContainerScore.AssiganedContainer(boardController.GetThePlayer1ScoreContainer());
            player2ContainerScore.AssiganedContainer(boardController.GetThePlayer2ScoreContainer());
        }
    }

}
