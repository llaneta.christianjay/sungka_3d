using Sungka.Controller.Sungka;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace Sungka.UI.Container
{
    public class ContainerDetailUI : MonoBehaviour
    {
        #region Config
        [SerializeField] TextMeshProUGUI textShellCounter;
        #endregion

        SungkaContainer sungkaContainer;
        const float VIBRATE_DURATION = 0.3f;
        const float VIBRATE_INTENSITY = 5f;
        public void AssiganedContainer(SungkaContainer sungkaContainer)
        {
            this.sungkaContainer = sungkaContainer;
            sungkaContainer.OnContainerUpdate += UpdateCount;
            UpdateCount();
        }

        private void UpdateCount()
        {
            textShellCounter.SetText(sungkaContainer.GetTheContainerShell().Count.ToString());
            StopCoroutine(VibrateTextRect());
            StartCoroutine(VibrateTextRect());
        }

        private IEnumerator VibrateTextRect()
        {
            float startTime = Time.time;
            float endTime = startTime + VIBRATE_DURATION;
            RectTransform rect = textShellCounter.GetComponent<RectTransform>();
            while (Time.time < endTime)
            {
                float randomOffset = Random.Range(-VIBRATE_INTENSITY, VIBRATE_INTENSITY);
                rect.anchoredPosition += new Vector2(randomOffset, randomOffset);
                yield return null;
                rect.anchoredPosition -= new Vector2(randomOffset, randomOffset);
            }
        }


    }


}
