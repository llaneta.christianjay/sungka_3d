using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Sungka.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UserInterface : MonoBehaviour
    {
        [Header("Config Data")]
        [Tooltip("Assign a button if it needs to toggle the fade in and fade out state of the UI.")]
        [SerializeField] Button toggleButton;

        [Header("UI General Properties")]
        [SerializeField] UserInterfaceType userInterfaceType = UserInterfaceType.Interactable;
        [SerializeField] InitialState initialState = InitialState.FadedIn;

        [Header("UI Fade Properties")]
        [SerializeField] float fadeInTime = 0.25f;
        [SerializeField] float transitionFadeWaitTime = 1f;
        [SerializeField] float fadeOutTime = 0.25f;
        [SerializeField] float initialFadeOutWaitTime = 5f;

        public event Action OnFadeIn;
        public event Action OnFadeOut;

        CanvasGroup canvasGroup;

        Coroutine currentActiveFaderCoroutine = null;
        bool isFadedOut;

        const int FADED_IN_ALPHA = 1;
        const int FADED_OUT_ALPHA = 0;

        enum UserInterfaceType
        {
            Interactable,
            NotInteractable
        }

        enum InitialState
        {
            FadedIn,
            FadedOut,
            FadedInThenFadeOut
        }

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        protected void Start()
        {
            switch (initialState)
            {
                case InitialState.FadedIn:
                    ShowFadedInState();
                    SetCanvasGroupProperties();
                    break;
                case InitialState.FadedOut:
                    ShowFadedOutState();
                    SetCanvasGroupProperties();
                    break;
                case InitialState.FadedInThenFadeOut:
                    ShowFadedInState();
                    StartCoroutine(InitialFadeOut());
                    break;
            }
        }

        private void OnEnable()
        {
            if (toggleButton != null)
            {
                toggleButton.onClick.AddListener(ToggleUI);
            }
        }

        private void OnDisable()
        {
            if (toggleButton != null)
            {
                toggleButton.onClick.RemoveListener(ToggleUI);
            }
        }

        public float GetTransitionFadeWaitTime()
        {
            return transitionFadeWaitTime;
        }

        #region "UI Fade Functionalities"
        public void ShowFadedInState()
        {
            canvasGroup.alpha = 1;
            isFadedOut = false;

            SetCanvasGroupProperties();
        }

        public void ShowFadedOutState()
        {
            canvasGroup.alpha = 0;
            isFadedOut = true;

            SetCanvasGroupProperties();
        }

        public IEnumerator InitialFadeOut()
        {
            yield return new WaitForSeconds(initialFadeOutWaitTime);
            StartFader(FADED_OUT_ALPHA, fadeOutTime);
        }

        public Coroutine FadeIn()
        {
            return StartFader(FADED_IN_ALPHA, fadeInTime);
        }

        public Coroutine FadeOut()
        {
            return StartFader(FADED_OUT_ALPHA, fadeOutTime);
        }

        public Coroutine FadeIn(float customFadeInTime)
        {
            return StartFader(FADED_IN_ALPHA, customFadeInTime);
        }

        public Coroutine FadeOut(float customFadeOutTime)
        {
            return StartFader(FADED_OUT_ALPHA, customFadeOutTime);
        }

        private Coroutine StartFader(float targetAlpha, float fadeTime)
        {
            if (currentActiveFaderCoroutine != null)
            {
                StopCoroutine(currentActiveFaderCoroutine);
            }

            currentActiveFaderCoroutine = StartCoroutine(StartFadeCoroutine(targetAlpha, fadeTime));
            return currentActiveFaderCoroutine;
        }

        private IEnumerator StartFadeCoroutine(float targetAlpha, float fadeTime)
        {
            float deltaAlpha = 0;

            while (!Mathf.Approximately(canvasGroup.alpha, targetAlpha))
            {
                deltaAlpha = Time.unscaledDeltaTime / fadeTime;
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, targetAlpha, deltaAlpha);
                yield return null;
            }

            if (Mathf.Approximately(canvasGroup.alpha, FADED_OUT_ALPHA))
            {
                isFadedOut = true;
            }
            else
            {
                isFadedOut = false;
            }

            if (OnFadeOut != null && isFadedOut == true)
            {
                OnFadeOut();
            }

            if (OnFadeIn != null && isFadedOut == false)
            {
                OnFadeIn();
            }

            SetCanvasGroupProperties();
        }

        private void SetCanvasGroupProperties()
        {
            if (userInterfaceType == UserInterfaceType.Interactable)
            {
                canvasGroup.interactable = !isFadedOut;
                canvasGroup.blocksRaycasts = !isFadedOut;
            }
            else
            {
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }
        }

        public void ToggleUI()
        {
            if (isFadedOut == true)
            {
                FadeIn();
            }
            else
            {
                FadeOut();
            }
        }
        #endregion
    }
}
