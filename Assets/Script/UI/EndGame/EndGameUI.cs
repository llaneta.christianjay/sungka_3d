using Sungka.Controller.Core;
using Sungka.Controller.Sungka;
using Sungka.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGameUI : MonoBehaviour
{
    #region Config
    [SerializeField] UserInterface uiEndGameScreen;
    [SerializeField] TextMeshProUGUI textSideWin;
    [SerializeField] TextMeshProUGUI player1Score;
    [SerializeField] TextMeshProUGUI player2Score;
    [SerializeField] SungkaBoardController boardController;
    [SerializeField] GameManager gameManager;
    #endregion
    void Awake()
    {
        gameManager.OnGameEnds += EndGameShow;
    }

    private void EndGameShow(int sideWind)
    {
        uiEndGameScreen.FadeIn();
        string whichSide = sideWind == 1 ? "Player 1 Win" : "Player 2 Win";
        textSideWin.SetText(whichSide);
        player1Score.SetText(boardController.GetThePlayer1ScoreContainer().GetTheContainerShell().Count.ToString());
        player2Score.SetText(boardController.GetThePlayer2ScoreContainer().GetTheContainerShell().Count.ToString());
    }
}
