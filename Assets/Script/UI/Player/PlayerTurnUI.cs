using Sungka.Controller.Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


namespace Sungka.UI.Player
{
    public class PlayerTurnUI : MonoBehaviour
    {
        #region Config
        [SerializeField] UserInterface uiTurnPopUp;
        [SerializeField] float showDuration = 2;
        [SerializeField] TextMeshProUGUI textShellCounter;
        [SerializeField] GameObject player1Banner;
        [SerializeField] GameObject player2Banner;
        [SerializeField] UserInterface player1Indicator;
        [SerializeField] UserInterface player2Indicator;
        [SerializeField] GameManager gameManager;
        #endregion


        private void Awake()
        {
            gameManager.OnGameChangeTurn += OnChangeTurn;
            gameManager.OnGameExtendTurn += OnExtendedTurn;
            ResetBanner();
        }
        private void ResetBanner()
        {
            player1Banner.SetActive(false);
            player2Banner.SetActive(false);
        }
        private void OnExtendedTurn()
        {
            ResetBanner();
            string whichSide = gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn ? "Player 1 Extended" : "Player 2 Extended";
            textShellCounter.SetText(whichSide);
            StartCoroutine(ShowPopUpScreen());
        }

        private void OnChangeTurn()
        {
            ResetBanner();
            string whichSide = gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn ? "Player 1 Turn" : "Player 2 Turn";
            textShellCounter.SetText(whichSide);
            StartCoroutine(ShowPopUpScreen());

        }

        IEnumerator ShowPopUpScreen()
        {

            uiTurnPopUp.FadeIn();
            yield return new WaitForSeconds(showDuration);
            uiTurnPopUp.FadeOut();
            player1Banner.SetActive(gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn);
            player2Banner.SetActive(gameManager.GetPlayerTurnState() == GameManager.TurnState.Player2Turn);
        }
    }
}

