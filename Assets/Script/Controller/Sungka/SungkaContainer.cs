using Sungka.Controller.Core;
using Sungka.Data.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sungka.Controller.Sungka
{
    public class SungkaContainer : MonoBehaviour
    {
        #region Config
        [SerializeField] Vector2 cointainerRange = new Vector2(1, 1.2f);
        [SerializeField] bool isPlayerScoringHole = false;
        #endregion

        #region Action
        public System.Action OnContainerUpdate;
        #endregion
        Transform Container;
        GameManager gameManager;
        BoxCollider interactionBox;
        GameManager.TurnState turnSide;
        private List<Shell> shellsInContained = new();

        private void Awake()
        {
            Container = GetComponent<Transform>();
            interactionBox = GetComponent<BoxCollider>();
        }

        public bool IsScoreContainer()
        {
            return isPlayerScoringHole;
        }
        private Vector3 GenerateRandomPoint()
        {

            float angle = Random.Range(0f, Mathf.PI * 2f);
            float distance = Random.Range(cointainerRange.x, cointainerRange.y);
            float xOffset = Mathf.Cos(angle) * distance;
            float zOffset = Mathf.Sin(angle) * distance;
            Vector3 randomPoint = Container.transform.position + new Vector3(xOffset, 0f, zOffset);

            return randomPoint;
        }

        public Quaternion GenerateRandomRotation()
        {
            // Generate random angles for rotation around each axis
            float randomAngleX = Random.Range(0f, 360f);
            float randomAngleY = Random.Range(0f, 360f);
            float randomAngleZ = Random.Range(0f, 360f);

            // Create a quaternion representing the random rotation
            Quaternion randomRotation = Quaternion.Euler(randomAngleX, randomAngleY, randomAngleZ);

            return randomRotation;
        }
        private void PutShellInTheContainer(Shell shell)
        {
            shell.transform.SetParent(Container);
            shell.transform.SetPositionAndRotation(GenerateRandomPoint(), GenerateRandomRotation());
        }

        private void OnTurnEnd()
        {
            interactionBox.enabled = (gameManager.GetPlayerTurnState() == turnSide);
        }

        public void PopulateStartingShell(GameManager gameManager, GameManager.TurnState turnSide)
        {
            this.turnSide = turnSide;
            this.gameManager = gameManager;
            GeneralSettings gameSettings = gameManager.GameSetting();
            for (int a = 0; a < gameSettings.GetDefaultStartingShell(); a++)
            {
                Shell shell = Instantiate(gameSettings.GetShellPrefab());
                shell.transform.SetParent(Container.transform);
                shell.transform.SetPositionAndRotation(Container.transform.position, Quaternion.identity);
                shell.SetShellColor(gameSettings.GetShellColors());
                AddShell(shell);
            }
            this.gameManager.OnGameChangeTurn += OnTurnEnd;
            OnTurnEnd();
        }



        public void AddShell(Shell shell)
        {
            shellsInContained.Add(shell);
            PutShellInTheContainer(shell);
            OnContainerUpdate?.Invoke();
        }

        public void RemoveShell(Shell shellToBeRemove)
        {
            shellsInContained.Remove(shellToBeRemove);
            OnContainerUpdate?.Invoke();
        }

        public void ClearedShell()
        {
            shellsInContained.Clear();
            OnContainerUpdate?.Invoke();
        }
        public List<Shell> GetTheContainerShell()
        {
            return shellsInContained;
        }


    }
}

