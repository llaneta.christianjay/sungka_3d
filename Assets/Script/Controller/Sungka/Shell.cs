using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sungka.Controller.Sungka
{
    public class Shell : MonoBehaviour
    {
        [SerializeField] Renderer shellRenderer;


        public void SetShellColor(List<Color> colorList)
        {
            shellRenderer.material.color = colorList[Random.Range(0, colorList.Count)];
        }
    }
}

