using Sungka.Controller.Core;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;




namespace Sungka.Controller.Sungka
{
    public class SungkaBoardController : MonoBehaviour
    {
        #region Config
        [SerializeField] List<SungkaContainer> player1Container = new();
        [SerializeField] List<SungkaContainer> player2Container = new();
        [SerializeField] SungkaContainer player1ScoreContainer;
        [SerializeField] SungkaContainer player2ScoreContainer;
        [SerializeField] GameObject fxScore;
        [SerializeField] GameObject fxInteract;
        [SerializeField] GameManager gameManager;
        #endregion

        #region Action
        public Action OnBoardPopulated;
        #endregion


        List<SungkaContainer> boardContainer = new();

        const float TRAVEL_SHELL_DURATION = 2;

        private void Awake()
        {
            gameManager.OnGameStart += InitializedBoard;

        }

        private void InitializedBoard()
        {
            //Populate player 1 shell
            foreach (var container in player1Container)
            {
                container.PopulateStartingShell(gameManager, GameManager.TurnState.Player1Turn);
                boardContainer.Add(container);
            }
            boardContainer.Add(player1ScoreContainer);

            //Populate player 2 shell
            foreach (var container in player2Container)
            {
                container.PopulateStartingShell(gameManager, GameManager.TurnState.Player2Turn);
                boardContainer.Add(container);
            }
            boardContainer.Add(player2ScoreContainer);
            OnBoardPopulated?.Invoke();
        }


        private bool CheckEndGame()
        {

            bool isP1Empty = true;
            bool isP2Empty = true;
            foreach (var container in player1Container)
            {
                if (container.GetTheContainerShell().Count > 0)
                    isP1Empty = false;
            }

            foreach (var container in player2Container)
            {
                if (container.GetTheContainerShell().Count > 0)
                    isP2Empty = false;
            }

            if (isP1Empty || isP2Empty)
                return true;
            return false;
        }

        private void GetEnemyContainer(SungkaContainer lastContainer)
        {

            SungkaContainer enemyContainer = null;
            int cointerIndex = 0;
            if (gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn)
            {
                cointerIndex = player1Container.IndexOf(lastContainer);
                if (cointerIndex >= 0)
                {
                    enemyContainer = player2Container[cointerIndex];
                }
                StartCoroutine(TravelShellFromHouseToScore(player1ScoreContainer, enemyContainer, lastContainer));



            }
            else if (gameManager.GetPlayerTurnState() == GameManager.TurnState.Player2Turn)
            {

                cointerIndex = player2Container.IndexOf(lastContainer);
                if (cointerIndex >= 0)
                {
                    enemyContainer = player1Container[cointerIndex];//6-cointerIndex
                }
                StartCoroutine(TravelShellFromHouseToScore(player2ScoreContainer, enemyContainer, lastContainer));


            }


        }

        #region OldGetEnemyContainer
        //private bool GetEnemyContainer(SungkaContainer lastContainer)
        //{

        //    SungkaContainer enemyContainer = null;
        //    int cointerIndex = 0;
        //    if (gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn)
        //    {
        //        cointerIndex = player1Container.IndexOf(lastContainer);
        //        if (cointerIndex >= 0)
        //        {
        //            enemyContainer = player2Container[cointerIndex];
        //        }
        //        StartCoroutine(TravelShellFromHouseToScore(player1ScoreContainer, enemyContainer));

        //        if (PassTheShellScore(player1ScoreContainer, enemyContainer))
        //        {
        //            Shell lastShell = lastContainer.GetTheContainerShell()[0];
        //            player1ScoreContainer.AddShell(lastShell);
        //            lastContainer.RemoveShell(lastShell);
        //            return true;
        //        }

        //    }
        //    else if (gameManager.GetPlayerTurnState() == GameManager.TurnState.Player2Turn)
        //    {

        //        cointerIndex = player2Container.IndexOf(lastContainer);
        //        if (cointerIndex >= 0)
        //        {
        //            enemyContainer = player1Container[cointerIndex];//6-cointerIndex
        //        }
        //        StartCoroutine(TravelShellFromHouseToScore(player1ScoreContainer, enemyContainer));

        //        if (PassTheShellScore(player2ScoreContainer, enemyContainer))
        //        {
        //            Shell lastShell = lastContainer.GetTheContainerShell()[0];
        //            player2ScoreContainer.AddShell(lastShell);
        //            lastContainer.RemoveShell(lastShell);
        //            return true;
        //        }

        //    }

        //    return false;
        //}
        #endregion


        IEnumerator TravelShellFromHouseToScore(SungkaContainer scoreContainer, SungkaContainer enemyContainer, SungkaContainer lastContainer)
        {

            if (enemyContainer != null && enemyContainer.GetTheContainerShell().Count > 0)
            {
                foreach (Shell shell in enemyContainer.GetTheContainerShell())
                {
                    float elapsedTime = 0;
                    Vector3 startingPosition = shell.transform.position;
                    Vector3 targetDestination = scoreContainer.transform.position;
                    while (elapsedTime < TRAVEL_SHELL_DURATION)
                    {
                        float t = Mathf.Clamp01(elapsedTime / TRAVEL_SHELL_DURATION);
                        Vector3 newPos = Vector3.Lerp(startingPosition, targetDestination, t);
                        newPos.y += 0.5f;
                        shell.transform.position = newPos;
                        elapsedTime += Time.deltaTime;
                        yield return null;
                    }
                    GameObject fx = Instantiate(fxScore);
                    fx.transform.SetPositionAndRotation(targetDestination, Quaternion.identity);
                }
                if (PassTheShellScore(scoreContainer, enemyContainer))
                {
                    Shell lastShell = lastContainer.GetTheContainerShell()[0];
                    scoreContainer.AddShell(lastShell);
                    lastContainer.RemoveShell(lastShell);
                    gameManager.ExtendTurn();
                }
                else
                {
                    gameManager.ChangeTurn();
                }
            }
            else
            {
                gameManager.ChangeTurn();
            }

        }
        private bool PassTheShellScore(SungkaContainer scoreContainer, SungkaContainer enemyContainer)
        {
            if (enemyContainer != null && enemyContainer.GetTheContainerShell().Count > 0)
            {
                foreach (Shell shell in enemyContainer.GetTheContainerShell())
                {
                    scoreContainer.AddShell(shell);
                }
                enemyContainer.ClearedShell();
                return true;
            }
            return false;
        }

        private void CheckLastContainerDrop(int lastContainerIndex)
        {
            SungkaContainer lastContainer = boardContainer[lastContainerIndex];
            if (CheckEndGame())
            {

                gameManager.EndGame();
                return;
            }
            //Check if falls to score container
            if (lastContainer.IsScoreContainer())
            {
                gameManager.ExtendTurn();
                Debug.Log("Turn Extended");
            }
            else if (lastContainer.GetTheContainerShell().Count == 1)
            {
                GetEnemyContainer(lastContainer);
                //if (GetEnemyContainer(lastContainer))
                //{
                //    gameManager.ExtendTurn();
                //    Debug.Log("enemy container pass Turn Extended");
                //}
                //else
                //{
                //    Debug.Log("turn change");
                //    gameManager.ChangeTurn();
                //}

            }
            else
            {
                Debug.Log("turn change");
                gameManager.ChangeTurn();
            }

        }
        public void ContainerToBePass(SungkaContainer selectedContainer)
        {

            GameObject fx = Instantiate(fxInteract);
            Vector3 interactPoint = selectedContainer.transform.position;
            interactPoint.y += 0.02f;
            fx.transform.SetPositionAndRotation(interactPoint, Quaternion.identity);
            StartCoroutine(TransferTheShellsContainer(selectedContainer));
        }


        IEnumerator TransferTheShellsContainer(SungkaContainer selectedContainer)
        {

            List<Shell> shellToPass = new();
            int indexStartingContainer = boardContainer.IndexOf(selectedContainer);

            shellToPass = boardContainer[indexStartingContainer].GetTheContainerShell();
            while (shellToPass.Count > 0)
            {
                SungkaContainer container = boardContainer[indexStartingContainer];
                Shell shell = shellToPass[shellToPass.Count - 1];
                Vector3 startingPosition = shell.transform.position;
                Vector3 targetDestination = container.transform.position;
                float elapsedTime = 0;

                while (elapsedTime < TRAVEL_SHELL_DURATION)
                {
                    float t = Mathf.Clamp01(elapsedTime / TRAVEL_SHELL_DURATION);
                    Vector3 newPos = Vector3.Lerp(startingPosition, targetDestination, t);
                    newPos.y += 0.5f;
                    shell.transform.position = newPos;
                    elapsedTime += Time.deltaTime;
                    yield return null;
                }
                if (container.IsScoreContainer())
                {
                    GameObject fx = Instantiate(fxScore);
                    fx.transform.SetPositionAndRotation(targetDestination, Quaternion.identity);
                }

                selectedContainer.RemoveShell(shell);
                container.AddShell(shell);
                indexStartingContainer++;
                if (indexStartingContainer >= boardContainer.Count)
                    indexStartingContainer = 0;
            }
            //do
            //{

            //    //  yield return null;
            //} while (shellToPass.Count > 0);
            int val = indexStartingContainer <= 0 ? boardContainer.Count - 1 : indexStartingContainer - 1;
            CheckLastContainerDrop(val);
        }

        public List<SungkaContainer> GetThePlayer1Container()
        {
            return player1Container;
        }
        public List<SungkaContainer> GetThePlayer2Container()
        {
            return player2Container;
        }

        public SungkaContainer GetThePlayer1ScoreContainer()
        {
            return player1ScoreContainer;
        }

        public SungkaContainer GetThePlayer2ScoreContainer()
        {
            return player2ScoreContainer;
        }



    }
}

