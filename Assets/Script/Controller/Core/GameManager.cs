using Sungka.Data.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sungka.Controller.Core
{
    public class GameManager : MonoBehaviour
    {
        public enum TurnState
        {
            Player1Turn,
            Player2Turn
        }
        #region Config
        [SerializeField] GeneralSettings generalSettings;
        #endregion

        #region Action
        public Action OnGameStart;
        public Action OnGameChangeTurn;
        public Action OnGameExtendTurn;
        public Action<int> OnGameEnds;
        #endregion


        private TurnState playerTurn;


        private void Start()
        {
            InitializedGame();
        }
        private void InitializedGame()
        {
            playerTurn = TurnState.Player1Turn;
            OnGameChangeTurn?.Invoke();
            OnGameStart?.Invoke();
        }

        public void ChangeTurn()
        {
            playerTurn = (playerTurn == TurnState.Player1Turn) ? TurnState.Player2Turn : TurnState.Player1Turn;
            OnGameChangeTurn?.Invoke();
        }
        public void ExtendTurn()
        {

            OnGameExtendTurn?.Invoke();
        }

        public void EndGame()
        {
            //string whichSide = gameManager.GetPlayerTurnState() == GameManager.TurnState.Player1Turn ? "Player 1 Turn" : "Player 2 Turn";
            int sideWin = playerTurn == TurnState.Player1Turn ? 0 : 1;
            OnGameEnds?.Invoke(sideWin);
        }
        public TurnState GetPlayerTurnState()
        {
            return playerTurn;
        }
        public GeneralSettings GameSetting()
        {
            return generalSettings;
        }
    }
}

