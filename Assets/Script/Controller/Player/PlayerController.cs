using Sungka.Controller.Core;
using Sungka.Controller.Sungka;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sungka.Controller.Player
{
    public class PlayerController : MonoBehaviour
    {
        #region Config
        [SerializeField] SungkaBoardController sungkaBoard;
        [SerializeField] GameManager gameManager;
        #endregion
        LayerMask ContainerLayer;
        bool isAllowInteraction = false;
        private void Awake()
        {
            gameManager.OnGameStart += InitializedController;
        }

        private void InitializedController()
        {
            isAllowInteraction = true;
            ContainerLayer = gameManager.GameSetting().GetSungkaContainerLayer();
        }

        private void InteractionCheck()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, ContainerLayer))
                {
                    SungkaContainer sungkaContainer = hit.collider.GetComponent<SungkaContainer>();
                    if (sungkaContainer != null)
                    {
                        sungkaBoard.ContainerToBePass(sungkaContainer);
                    }
                }
            }
        }
        private void Update()
        {
            if (isAllowInteraction)
            {
                InteractionCheck();
            }
        }
    }

}
